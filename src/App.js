import './App.css';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom'
import Home from './Components/Home';
import Login from './Components/Login';
import Contactus from './Components/Contactus';
import Aboutus from './Components/Aboutus';
import Navigationbar from './Components/Navigationbar';

function App() {
  return (
    <div>


      <div>
        <BrowserRouter>
        <Navigationbar />
        
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/contactus" element={<Contactus />} />
            <Route path="/aboutus" element={<Aboutus />} />



          </Routes>
        </BrowserRouter>

      </div>
    </div>
  );
}

export default App;
