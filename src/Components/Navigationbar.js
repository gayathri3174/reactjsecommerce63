import React from 'react'
import { Link } from 'react-router-dom'
import './Navigationbar.css'
import Logo from '../images/Logo.png'
import LoginPage from './LoginPage'
import Register from './Register'



function Navigationbar() {
    const navbardesign = {
        backgroundColor: 'gray',
        color: 'white'
    }
    return (
        <div>

            <div className='navbarstyle d-flex justify-content-between ' style={{ border: '2px solid black' }}   >
                <h1>Logo <img style={{ height: '50px', width: '50px', borderRadius: '50%' }} src={Logo} alt="logo" />
                </h1>
                <div className='d-flex ' style={{ border: "1px solid black" }}>
                    <Link className="m-2 text-decoration-none" to="/">Home Page</Link>
                    <Link className="m-2 text-decoration-none" to="/login">Login</Link>
                    <Link to="/contactus" className="m-2 text-decoration-none" >Contact us</Link>
                    <Link to="/aboutus" className="m-2 text-decoration-none"> About Us</Link>
                    <LoginPage />
                    <Register />

                  

                </div>
            </div>

        </div>
    )
}

export default Navigationbar