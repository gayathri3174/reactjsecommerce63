import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,FormGroup,Label,Input, } from 'reactstrap';
import { Glyphicon } from 'react-bootstrap';
import axios from 'axios'
function LoginPage(args) {
  const [modal, setModal] = useState(false);

  let [email,setEmail] = useState("")
  let [password,setPassword] = useState("")
 
  const handleSubmit = async (e) =>{
    e.preventDefault();
    // alert(email + " "+password)

    try{
      let response = await axios.post("http://localhost:3005/login",{"email":email, "password":password})
      console.log(response)
      alert(response.data)

    }catch(err){
      console.log(err)
    }
    
 
    

  }

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="primary" onClick={toggle}>
       Login 
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Login </ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}> ,
  <FormGroup>
    <Label
      for="exampleEmail"
      hidden
    >
      Email
    </Label>
    <Input
      id="exampleEmail"
      name="email"
      placeholder="Email"
      type="email"
      required
      value={email}
      onChange={(e)=>{
        setEmail(e.target.value);}}
    />
     
  </FormGroup>
  {' '}
  <FormGroup>
    <Label
      for="examplePassword"
      hidden
    >
      Password
    </Label>
    <Input
      id="examplePassword"
      name="password"
      placeholder="Password"
      type="password"
      value={password}
      onChange={(e)=>{setPassword(e.target.value)}}
      required
    />
  </FormGroup>
  {' '}
  <Button type="submit">
    Submit
  </Button>
</Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default LoginPage ;