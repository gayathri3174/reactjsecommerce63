import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {Label,Input,Col,Row,FormGroup,Form} from 'reactstrap'
function Register(args) {
  const [modal, setModal] = useState(false);
  let [formdata, setFormdata] = useState({
    email:"",
    password:"",
    address:"",
    address2:"",
    city:"",
    state:"",
    zip:""

  })

  const handleSubmit = (e) =>{
    e.preventDefault()
    alert("registered")
    console.log(formdata)
  }

  const handleFormdata = (e) =>{
    let {name,value} = e.target
      
  setFormdata({
   ...formdata,
   [name]:value
  })

  // console.log(formdata)
  }

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Click Me
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="with a placeholder"
          type="email"
          value={formdata.email}
          onChange={handleFormdata}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password placeholder"
          type="password"
          value={formdata.password}
          onChange={handleFormdata}
          required

        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="1234 Main St"
      value={formdata.address}
      onChange={handleFormdata}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleAddress2">
      Address 2
    </Label>
    <Input
      id="exampleAddress2"
      name="address2"
      placeholder="Apartment, studio, or floor"
      value={formdata.address2}
      onChange={handleFormdata}
      required
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleCity">
          City
        </Label>
        <Input
          id="exampleCity"
          name="city"
          value={formdata.city}
          onChange={handleFormdata}
          required
        />
      </FormGroup>
    </Col>
    <Col md={4}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          value={formdata.state}
          onChange={handleFormdata}
          required
        />
      </FormGroup>
    </Col>
    <Col md={2}>
      <FormGroup>
        <Label for="exampleZip">
          Zip
        </Label>
        <Input
          id="exampleZip"
          name="zip"
          value={formdata.zip}
          onChange={handleFormdata}
          required
        />
      </FormGroup>
    </Col>
  </Row>

  <Button type='submit'>
     Register
  </Button>
</Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Register;